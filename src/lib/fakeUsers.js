export const fakeUsers = [
  {
    honorific: 0,
    firstName: 'Jake',
    lastName: 'Timberland',
    email: 'jtimberland@hotmail.com',
    dateOfBirth: new Date(1998, 12, 2),
    phoneNumber: '120-56494-891',
  },
  {
    honorific: 1,
    firstName: 'Mary',
    lastName: 'Ashton',
    email: 'ashton.mary@protonmail.com',
    dateOfBirth: new Date(1995, 5, 4),
    phoneNumber: '120-15995-256',
  },
  {
    honorific: 2,
    firstName: 'Anderson',
    lastName: 'Yteval',
    email: 'ytv.ander@gmail.com',
    dateOfBirth: new Date(1987, 1, 3),
    phoneNumber: '120-88615-562',
  },
];
